
# Sylva-library

A library chart containing generic named templates reusable from other sylva helm charts like 
[sylva-units](https://gitlab.com/sylva-projects/sylva-core/-/tree/main/charts/sylva-units) and 
[sylva-capi-cluster](https://gitlab.com/sylva-projects/sylva-elements/helm-charts/sylva-capi-cluster).

To use them, declare it as a dependency in the corresponding Helm chart.

## local development step

We can distinguish 2 cases:

* development on a parent Helm chart that require modification on sylva-library

* development on a parent Helm chart that doesn't require modification on sylva-library

### modification on sylva-library

We suppose you have already cloned the sylva-library code.

In the Chart.yaml file of the parent Helm Chart (for example sylva-core) add/change the following section

```
dependencies:
  - name: sylva-library
    version: ">=0.0.1"
    repository: file://../../../sylva-projects/sylva-elements/helm-charts/sylva-library/
```

replace repository `file://` value with the path corresponding to your local clone of sylva-library source code

**warning**: before executing any "helm template" command you must update the dependencies if any change occurs in the dependency library

to update dependencies, execute `helm dependencies update <path-to-your-chart>` 

### modification on Helm chart that depends on sylva-library

In case you don't have to modify the sylva-library code, keep the dependencies pointing to Helm repository (gitlab url).

Don't forget to run at least once `helm dependencies update <path-to-your-chart>` before executing any `helm template ...`


## gitlab development step

When you test your change in the gitlab CI, you need to change de repository of the dependency:

In the Chart.yaml file change the dependency
```
dependencies:
  - name: sylva-library
    version: 0.0.0-api-version
    repository: https://gitlab.com/api/v4/projects/56163211/packages/helm/base
```

version: must correspond to a tag of sylva-library

repository: must point to sylva-projects Helm repository https://gitlab.com/api/v4/projects/56163211/packages/helm/base

## gitlab production step

Compared to gitlab development step, use instead a production tag.

## Filename Convention

In Helm charts of type library, all files containing name template definitions must begin with an underscore ("_") to ensure they are accessible from the parent chart.