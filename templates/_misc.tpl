{{/*

k8s-flavor-from-cabpX

returns the nice name corresponding to a capbX CAPI codename for
a CAPI bootstrap/control-plane provider

  cabpk   -> kubeadm
  cabpr   -> rke2
  cabpoa  -> openshift
  cabpck  -> ck8s

*/}}
{{- define "k8s-flavor-from-cabpX" -}}
  {{- $cabpX := . -}}
  {{- if $cabpX | eq "cabpk" -}}
    kubeadm
  {{- else if $cabpX | eq "cabpr" -}}
    rke2
  {{- else if $cabpX | eq "cabpoa" -}}
    openshift
  {{- else if $cabpX | eq "cabpck" -}}
    ck8s
  {{- else -}}
    {{- fail (printf "k8s-flavor-from-cabpX: unknown k8s-flavor for CAPI bootstrap provider '%s'" $cabpX) -}}
  {{- end -}}
{{- end -}}
