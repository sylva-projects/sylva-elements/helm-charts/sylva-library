{{/*
----------------------------------------------------------------------------------------------------------
This file defines named-template helpers to allow sylva-capi-cluster and
sylva-units to automatically select OS images among a list of OS images,
based on their properties.

It also defines a few other additional helpers.

The properties of an os image is typically built from annotations
of the OCI artifacst built from sylva diskimage-builder, which are gathered
by the os-images-info unit into a ConfigMap which is then fed into sylva-capi-cluster.

But those properties could be added by other tools.

Example:

    os_images:

      ubuntu-jammy-hardened-rke2-1-29-8:
        archive-size: "1962836783"
        commit-id: 7d5b196578e9803602e6b5770a48fd8eb22ffdba
        commit-tag: 0.3.0
        compression: gz
        filename: ubuntu-jammy-hardened-rke2-1-29-8.raw
        flavor: hardened
        hardened: "true"
        image-format: raw
        k8s-flavor: rke2
        k8s-version: 1.29.8-rke2r1
        md5: c4040abdbd759e596f2f014d5e07c064
        os: ubuntu
        os-release: jammy
        size: "22000000000"
        uri: oci://registry.gitlab.com/sylva-projects/sylva-elements/diskimage-builder/ubuntu-jammy-hardened-rke2-1-29-8:0.3.0

      opensuse-15-5-plain-rke2-1-29-9:
        archive-size: 1680340771,
        commit-id: 4649471800159e98ef03b6de0dab937867e85507,
        commit-tag: 0.3.1,
        compression: gz,
        filename: opensuse-15-5-plain-rke2-1-29-9.raw,
        flavor: plain,
        hardened: false,
        image-format: raw,
        k8s-flavor: rke2,
        k8s-version: 1.29.9-rke2r1,
        md5: 3a869cad31dc776e180e089476da0b6a,
        os: opensuse,
        os-release: 15.5,
        sha256: cb3c4870f4e714a40977f3388a8eb5694c08f73d542eaa9aa15a0bf4c23430a1,
        size: 22000000000

An "OS image selector" is a dictionary matching key/values in entries of 'os_images'.

For instance, this selector:

  k8s-version: 1.29.9-rke2r1
  k8s-flavor: rke2
  os: opensuse
  os-release: 15.5

... would result in selecting the 'opensuse-15-5-plain-rke2-1-29-9' image key.

a selector value can also be a regex:

  k8s-version: 1.29.9-rke2.*
  k8s-flavor: kubeadm
  os: opensuse
  os-release: (15.5|15.6)


!! Sylva documentation portal has an "OS image selectors" explaining all this with more details !!

----------------------------------------------------------------------------------------------------------
*/}}

{{/*

validate-selector

This named template validates a dict containing an os-selector:

It ensures that:
* it has only well-formed official keys
  * OR that the user explicitely opted-in to use unofficial ones
    (this is done by setting  the "_use_extra_keys_" special key)
* values are either non-empty strings or booleans

If one of these criteria isn't met, the template fail's
with an error message.

*/}}
{{- define "validate-selector" -}}
  {{- $selector := . -}}

  {{- $base_keys := list
     "_use_extra_keys_"
     "_os_images_key_"
     "os"
     "os-release"
     "k8s-flavor"
     "k8s-version"
     "hardened"
     "commit-tag"
     "flavor"
  -}}
  {{- range $selector_key, $selector_value := $selector -}}
    {{- if and (not ($base_keys | has $selector_key)) (not (hasKey $selector "_use_extra_keys_")) -}}
      {{- fail (printf "OS selector key '%s': this key is not among the supported base keys. Supported keys are: %s. (check documentation if you really need to use a key which isn't in supported base keys)"
                       $selector_key ($base_keys| join ", ")
               ) -}}
    {{- end -}}
    {{- if not (or ($selector_value | kindIs "string") (or ($selector_value | kindIs "bool"))) -}}
      {{- fail (printf "OS selector has unsupported type for key '%s', match value '%v' is not a string or a boolean" $selector_key $selector_value) -}}
    {{- end -}}
  {{- end -}}
{{- end -}}



{{/*

selector-matches-os-image

This named template returns "true" or "" (false) depending
on whether the selector matches the submitted image.

Parameters
- os_image (dict)
- image_selector

A selector matches an image if and only if for all keys of the selector
the corresponding attribute of the image matches the selector regex.

Notes:
* boolean values are automatically changed into strings
* a regex has to match the whole value (e.g "os: foo[12]" in the selector would match "foo1", but not "afoo14")
* the special "_use_extra_keys_" is ignored (see validate-selector about its use)

*/}}
{{- define "selector-matches-os-image" -}}
  {{- $os_image := index . 0 -}}
  {{- $selector := index . 1 -}}

  {{- $result := true -}}
  {{- range $selector_key, $selector_value := $selector -}}

    {{- if not ($selector_key | kindIs "string") -}}
      {{- fail (printf "selector key %s is not a string (%s)" $selector_key (kindOf $selector_key)) -}}
    {{- end -}}

    {{- if $selector_key | eq "_use_extra_keys_" -}}
      {{/* special key only used to opt-in to use unofficial keys, see validate-selector */}}
      {{- continue -}}
    {{- end -}}

    {{/* cast bool as strings */}}
    {{- if $selector_value | kindIs "bool" -}}
      {{- $selector_value = $selector_value | toString -}}
    {{- end -}}

    {{- if not ($selector_value | kindIs "string") -}}
      {{- fail (printf "selector value for key %s is not a string: %s:%s" $selector_key $selector_value (kindOf $selector_value)) -}}
    {{- end -}}

    {{- $candidate := $os_image | dig $selector_key "//unset" -}}

    {{/* cast bool as strings */}}
    {{- if $candidate | kindIs "bool" -}}
      {{- $candidate = $candidate | toString -}}
    {{- end -}}

    {{- if not ($candidate | kindIs "string") -}}
      {{- fail (printf "OS attribute for key %s is not a string: %s:%s" $selector_key $candidate (kindOf $candidate)) -}}
    {{- end -}}

    {{/* match candidate against regex (after anchoring regex to start/end of line) */}}
    {{- if not ($candidate | regexMatch (printf "^(%s)$" $selector_value)) -}}
      {{- $result = false -}}
      {{- break -}}
    {{- end -}}{{/* if */}}

  {{- end -}}{{/* range */}}

  {{- if $result -}}
  true
  {{- else -}}{{/* empty string evaluates as false*/}}
  {{- end -}}
{{- end -}}


{{/*

select-os-images

This named template returns a dict (image key -> image dict), based on the following inputs:
- os_images (dict)
- selectors (list)

It will loop trough all the OS images in os_images, selecting
the one(s) that have key/values matching the ones specified in any of the selectors.

It will return an OS images dict with all those images (as JSON).

*/}}
{{- define "select-os-images" -}}
  {{- $os_images := index . 0 -}}
  {{- $selectors := index . 1 -}}

  {{- if not ($os_images | kindIs "map") -}}
    {{- fail (printf "select-os-images: os_images is not a map (is a %s)" (kindOf $os_images)) -}}
  {{- end -}}

  {{- if $os_images | keys | len | eq 0 -}}
    {{- fail (printf "select-os-images: os_images is empty, fail") -}}
  {{- end -}}

  {{- $matches := dict -}}
  {{- range $selector :=  $selectors}}

    {{- include "validate-selector" $selector -}}

    {{- range $image_key, $os_image := $os_images -}}
      {{- $_ := set $os_image "_os_images_key_" $image_key -}}{{/* '_os_images_key_' is a special attribute which is the key os os_image for this image */}}
      {{- if (tuple $os_image $selector | include "selector-matches-os-image") -}}
        {{- $_ := set $matches $image_key $os_image -}}
      {{- end -}}
    {{- end -}}
  {{- end -}}

  {{- $matches | toJson -}}
{{- end -}}


{{/*

pick-os-image

This named template returns an image_key, based on the following inputs:
- os_images (dict)
- selectors (list of image_selector)
- fallback_image_key (string)

It will loop trough all the OS images in os_images, selecting
the one(s) that have key/values matching the ones specified in any of the selectors.

It will return:
* one image_key if a single matching OS image was found
* fallback_image_key if none is found (or fail if fallback_image_key isn't specified)
* (fail if more than one image is found)

*/}}
{{- define "pick-os-image" -}}
  {{- $os_images := index . 0 -}}
  {{- $selectors := index . 1 -}}
  {{- $fallback_image_key := index . 2 -}}
  {{- $context := index . 3 -}}{{/* used for error messages */}}

  {{- $image_keys := tuple $os_images $selectors | include "select-os-images" | fromJson | keys -}}

  {{- if ($image_keys | len | eq 1) -}}
    {{- index $image_keys 0 -}}
  {{- else if and ($image_keys | len | eq 0) $fallback_image_key -}}
    {{- $fallback_image_key -}}
  {{- else if ($image_keys | len | eq 0) -}}
    {{- fail (printf "%s: no OS image could be found matching selectors\n\nselectors:%s\n\nOS images:%s" $context ($selectors|toYaml|nindent 2) ($os_images|toYaml|nindent 2)) -}}
  {{- else -}}{{/* more than one */}}
    {{- fail (printf "%s: we need one OS image, but %d were found matching selectors %s (%s)" $context ($image_keys | len) ($selectors|toJson) ($image_keys | join ",")) -}}
  {{- end -}}

{{- end -}}


{{/* finalize-os-image-selector

Helper taking an OS image selector dict as input and output
as JSON, the same selector with:

- the cluster k8s version injected into it as "k8s-version" (unless it's already specified)
  any leading "v" is removed

- the kubernetes flavor, determined based  on CAPI bootstrap provider
  injected as "k8s-flavor"

Those "k8s-version" and "k8s-flavor" keys are consistent with the annotations
set on OCI image artifacts by sylva diskimage-builder.

The first parameter, $clusterValues, is a dict containing 'k8s_version' and 'capi_providers'.
(from sylva-core .Values.cluster is used, from sylva-capi-cluster .Values is used)

e.g.:

$selector := tuple $envAll.Values $selector | include "finalize-os-image-selector" | fromJson -}}

*/}}
{{- define "finalize-os-image-selector" -}}
  {{- $clusterValues := index . 0 -}}
  {{- $selector := index . 1 -}}

  {{- mergeOverwrite (dict "k8s-version" ($clusterValues.k8s_version | trimPrefix "v" | replace "+" "\\+")
                           "k8s-flavor"  (include "k8s-flavor-from-cabpX" $clusterValues.capi_providers.bootstrap_provider)
                     )
                     (deepCopy $selector)
    | toJson -}}
{{- end -}}



{{- define "single-def-cluster-image-selector" -}}
  {{- $clusterValues := index . 0 -}}
  {{- $def := index . 1 -}}

  {{- $selector := dict -}}
  {{- if $def.os_image_selector -}}
    {{- $selector = tuple $clusterValues $def.os_image_selector | include "finalize-os-image-selector" | fromJson -}}
  {{- end -}}

  {{- if $def.image_key -}}
    {{- $_ := set $selector "_os_images_key_" $def.image_key -}}
  {{- end -}}

  {{- include "validate-selector" $selector -}}

  {{- if $selector -}}{{/* return result */}}
    {{- list $selector | toJson -}}
  {{- else -}}{{/* or nothing */}}
  []
  {{- end -}}
{{- end -}}


{{/*

*/}}
{{- define "find-cluster-image-selectors" -}}
  {{- $clusterValues := index . 0 -}}

  {{- $infra_provider := $clusterValues.capi_providers.infra_provider -}}

  {{- $selectors := list -}}

  {{/* get .cluster.(capo|capm3) definition */}}
  {{- $rootDef := ($clusterValues | dig $infra_provider dict) -}}

  {{/* add .cluster.control_plane.(capo|capm3) OS image selector */}}
  {{- $cpDef := mergeOverwrite (deepCopy $rootDef)
                               (deepCopy ($clusterValues | dig "control_plane" $infra_provider dict)) -}}
  {{- $selectors = concat $selectors (tuple $clusterValues $cpDef | include "single-def-cluster-image-selector" | fromJsonArray) -}}

  {{/* get .(machine_deployment_default|machine_deployments.xx).(capo|capm3) OS image selectors */}}
  {{- $mdDefaultDef := mergeOverwrite dict (deepCopy $rootDef)
                                           (deepCopy ($clusterValues | dig "machine_deployment_default" $infra_provider dict)) -}}
  {{- range $mdName, $mdDef := $clusterValues.machine_deployments -}}
    {{- $mdDef := mergeOverwrite dict (deepCopy $mdDefaultDef)
                                      (deepCopy ($mdDef | dig $infra_provider dict)) -}}
    {{- $selectors = concat $selectors (tuple $clusterValues $mdDef | include "single-def-cluster-image-selector" | fromJsonArray) -}}
  {{- end -}}

  {{/* return result */}}
  {{- $selectors | toJson -}}
{{- end -}}


{{/* compute-image-key

This named template compute an image key from:
* ($def, 1st parameter) an entry of sylva-capi-cluster configuration level, expected to contain an "os_image_selector" field and/or an "image_key" field
  examples of such "levels":
  - capo
  - control_plane.capo
  - capm3
  - machine_deployments.xxxx.capm3

* ($clusterValues, 2nd parameter) a values dict (in the format of sylva-capi-cluster values), expected to contain a k8s_version
  and capi_providers.bootstrap_provider   (from sylva-core .Values.cluster is used, from sylva-capi-cluster .Values is used)

* ($os_images, 3rd parameter) an OS images dict

* (a context string to produce useful error messages)

The output of the named template is an image key string:
* if an "os_image_selector" is found in $def, an image matching this selector is searched in the OS images dict
* if the "image_key" field is set in $def it is used
* if bot are set, an error is raised if then don't compute the same result
* (if none is set, an empty string is returned)

This is used to compute the image key to use for a given level of sylva-capi-cluster configuration.
For instance, the 'capo' key, or the 'control_plane.capm3' key.

*/}}
{{- define "compute-image-key" -}}
  {{- $def := index . 0 -}}
  {{- $clusterValues := index . 1 -}}
  {{- $os_images := index . 2 -}}
  {{- $context := index . 3 -}}{{/* this is just used to display useful error messages */}}

  {{- $image_key := "" -}}
  {{- if $def.os_image_selector -}}
    {{- $os_image_selector := tuple $clusterValues $def.os_image_selector | include "finalize-os-image-selector" | fromJson -}}
    {{- $image_key = tuple $os_images (list $os_image_selector) "" $context | include "pick-os-image" -}}

    {{- if and $def.image_key (not (eq $def.image_key $image_key)) -}}
      {{- fail (printf "%s: both .os_image_selector and .image_key are set, but the resulting image key differ (specified image_key: %s, image_key resulting from os_image_selector: %s)" $context $def.image_key $image_key) -}}
    {{- end -}}
  {{- else -}}
    {{- $image_key = $def | dig "image_key" "" -}}
  {{- end -}}
  {{- $image_key -}}
{{- end -}}


{{/*

os-image-pv-size

this named template computes the PV size required for the OS image, unit is Mi (Mebibytes)

- it takes into account the need to eventually store at the same
  time the compresssed image and the uncompressed image

- ~3% of overhead because, at least for some CSI providers, the PV size
  is the block-disk size of the backind device, ot the usable filesystem
  size

*/}}
{{- define "_os-image-pv-size" -}}
  {{- $os_image := index . 0 -}}
  {{- $account_for_decompressed_image := index . 1 -}}

  {{- $Mi := mul 1024 1024 -}}
  {{- $image_size := $os_image.size | default 0 | int -}}
  {{- $required_disk_size := 0 -}}
  {{- $archive_size := index $os_image "archive-size" | default 0 | int -}}
  {{- if and (eq ($os_image | dig "compression" "none") "gz")
             (not $account_for_decompressed_image) -}}
    {{- /* No need to add image size in that case, as it won't be uncompresed */ -}}
    {{- $required_disk_size = add $archive_size (mul 100 $Mi) -}}
  {{- else -}}
    {{- /* the size we need on disk is the archive size plus the uncompressed image size */ -}}
    {{- $required_disk_size = add $image_size $archive_size (mul 100 $Mi) -}}
  {{- end -}}

  {{/* the PV size needs, at least with cinder-csi, to account for the filesystem overhead,
      here estimated at ~3% for ext4 (with some margin)
      and we round _up_ to the nearest gigabyte
  */}}
  {{- $fs_usable_ratio := 0.97 -}}
  {{- $pv_size_Mi := divf (divf $required_disk_size $fs_usable_ratio) $Mi | ceil -}}

  {{- $pv_size_Mi -}}
{{- end -}}

{{- define "os-image-pv-size" -}}
  {{- $os_image := . -}}
  {{- include "_os-image-pv-size" (tuple $os_image false) -}}
{{- end -}}

{{- define "os-image-pv-size-decompressed" -}}
  {{- $os_image := . -}}
  {{- include "_os-image-pv-size" (tuple $os_image true) -}}
{{- end -}}
