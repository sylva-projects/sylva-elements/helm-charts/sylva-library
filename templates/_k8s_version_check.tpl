{{/*

k8s-version-match

Tests whether the cluster k8s version matches semver constraints.

Usage (in some values.yaml file of sylva-units and s-c-c templates):

  foo: {{ include "k8s-version-match" (tuple ">1.29.9,<=1.30.5" .Values.cluster.k8s_version )}}

Result (in sylva-units final rendering, where boolean typing will have been preserved):

  foo: true  # or false if the version is not between 1.29.9 and 1.30.5

*/}}

{{ define "k8s-version-match" }}
  {{- $match := index . 0 -}}
  {{- $comparison := index . 1 -}}
  {{/* need to remove the prerelease part of semver version (details in https://gitlab.com/sylva-projects/sylva-core/-/merge_requests/2467) */}}
  {{- if semverCompare $match (regexReplaceAll "-[^+]*" $comparison "") -}}
    true
  {{- else -}}{{/* false encoded as empty string */}}
  {{- end -}}
{{ end }}
