{{/*
This named template returns an error if the context passed in argument fails a check for any of:

- .cluster_virtual_ip is inside the subnet of .capm3.primary_pool_network, for empty .metallb.bgp_lbs and .kube_vip.bgp_lbs
- .cluster_virtual_ip is not equal to .capm3.primary_pool_gateway
- .cluster_virtual_ip is not inside the range defined by .capm3.primary_pool_start and .capm3.primary_pool_end

It can be used in parent charts like:

    * `sylva-capi-cluster` with:
        {{ tuple .Values | include "capm3NetworkValidation" }}

    * `sylva-units` with:
        {{- $envAll := set . "Values" (include "interpret-values-gotpl" . | fromJson) -}}
        {{ tuple $envAll.Values.cluster | include "capm3NetworkValidation" }}

*/}}
{{- define "capm3NetworkValidation" -}}
{{- $values := index . 0 -}}
    {{- $cluster_virtual_ip   := $values.cluster_virtual_ip -}}
    {{- $primary_pool_network := $values.capm3.primary_pool_network -}}
    {{- $primary_pool_prefix  := $values.capm3.primary_pool_prefix -}}
    {{- $primary_pool_start   := $values.capm3.primary_pool_start -}}
    {{- $primary_pool_end     := $values.capm3.primary_pool_end -}}
    {{- $primary_pool_gateway := $values.capm3.primary_pool_gateway -}}

    {{- $net_bits  := $primary_pool_prefix | atoi -}}

    {{- $cluster_virtual_ip_decimal   := tuple $cluster_virtual_ip   | include "get-ipv4-decimal" -}}
    {{- $primary_pool_network_decimal := tuple $primary_pool_network | include "get-ipv4-decimal" -}}

    {{/* checking .cluster_virtual_ip is inside the subnet of .capm3.primary_pool_network,
         skipping it if using BGP (.metallb.bgp_lbs or .kube_vip.bgp_lbs), and not ARP, for its advertisement */}}
    {{- if not (or ($values.metallb).bgp_lbs ($values.kube_vip).bgp_lbs) -}}
      {{/* we compare the most significant ($primary_pool_prefix) x bits, that define the network
           by formatting the IPv4 decimal representation to 32-bit binary string and truncating network bits */}}
      {{- $cluster_virtual_ip_net_bits   := trunc ($net_bits | int) ($cluster_virtual_ip_decimal   | atoi | printf "%032b") -}}
      {{- $primary_pool_network_net_bits := trunc ($net_bits | int) ($primary_pool_network_decimal | atoi | printf "%032b") -}}
      {{- if ne $cluster_virtual_ip_net_bits $primary_pool_network_net_bits -}}
        {{ fail (printf "The IP used to expose kube-api and LoadBalancer services (.cluster_virtual_ip) '%s' is not from subnet of .capm3.primary_pool_* '%s/%s'" $cluster_virtual_ip $primary_pool_network $primary_pool_prefix) }}
      {{- end -}}
    {{- end -}}

    {{/* checking .cluster_virtual_ip is not equal to .capm3.primary_pool_gateway */}}
    {{- if eq $cluster_virtual_ip $primary_pool_gateway -}}
      {{ fail (printf "The IP used to expose kube-api and LoadBalancer services (.cluster_virtual_ip) '%s' is also used as gateway for subnet of .capm3.primary_pool_* '%s/%s'" $cluster_virtual_ip $primary_pool_network $primary_pool_prefix) }}
    {{- end -}}

    {{/* checking .cluster_virtual_ip is outside of .capm3.primary_pool_start and .capm3.primary_pool_end range */}}
    {{/* OKD single node is an exception, its primary address will used be the .cluster_virtual_ip */}}
    {{- if and (eq $values.capi_providers.bootstrap_provider "cabpoa") (eq (int $values.control_plane_replicas) 1) -}}
      {{- $matchLabels := $values.control_plane.capm3.hostSelector.matchLabels }}
      {{- if eq (len $matchLabels) 0 }}
        {{ fail "[ERROR] control_plane.capm3.hostSelector.matchLabels is empty" }}
      {{- end }}
      {{- $found := false }}
      {{- range $name, $node := $values.baremetal_hosts }}
        {{- $labelMatches := true }}

        {{/* Check if all matchLabels are present in node labels with correct values */}}
        {{- range $labelKey, $labelValue := $matchLabels }}
          {{- if ne (index $node.bmh_metadata.labels $labelKey) $labelValue }}
            {{- $labelMatches = false }}
            {{- break }}
          {{- end }}
        {{- end }}
        {{- if and $labelMatches (eq $node.ip_preallocations.primary $cluster_virtual_ip) }}
          {{- $found = true }}
          {{- break }}
        {{- end }}
      {{- end }}
      {{- if not $found }}
        {{ fail (printf "'cluster_virtual_ip' for single node OKD/OpenShift cluster should be the same as the node primary ip address and 'control_plane' needs to have label matching baremetal_host") }}
      {{ end }}
    {{- else -}}
      {{- $primary_pool_start_decimal := tuple $primary_pool_start | include "get-ipv4-decimal" -}}
      {{- $primary_pool_end_decimal   := tuple $primary_pool_end   | include "get-ipv4-decimal" -}}
      {{- if and (ge $cluster_virtual_ip_decimal $primary_pool_start_decimal)
                 (le $cluster_virtual_ip_decimal $primary_pool_end_decimal) -}}
        {{ fail (printf "The IP used to expose kube-api and LoadBalancer services (.cluster_virtual_ip) '%s' is inside the range of .capm3.primary_pool_* '%s-%s'. This is wrong, as it may be allocated to a baremetal node." $cluster_virtual_ip $primary_pool_start $primary_pool_end) }}
      {{- end -}}
    {{- end -}}
{{- end -}}

{{/*
For an input IPv4 address represented in binary string, dotted decimal format A.B.C.D,
compute its 32-bit integer decimal representation, using the formula:
A * 16777216 (2 power 24) + B * 65536 (2 power 16) + C * 256 (2 power 8) + D * 1 (2 power 0)

Template:

{{- include "get-ipv4-decimal" 192.168.130.30 }}

Result:

3,232,268,830

*/}}
{{- define "get-ipv4-decimal" -}}
  {{- $ipv4 := index . 0 -}}
  {{- $ipv4_octets := $ipv4 | split "." -}}
  {{- add
      (($ipv4_octets._0 | atoi) | mul 16777216)
      (($ipv4_octets._1 | atoi) | mul 65536)
      (($ipv4_octets._2 | atoi) | mul 256)
       ($ipv4_octets._3 | atoi)
  -}}

{{- end -}}
