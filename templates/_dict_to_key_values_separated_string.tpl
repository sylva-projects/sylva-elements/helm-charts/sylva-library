{{/*
dict_to_key_values_separated_string: Transform a dict in a string

Use this function, when the target field is a string and you want the values in your helm chart to be a dict.
This will allow better override of helm chart value and benefit from the functionnality of mergeOverwrite.
This function allow you to choose the separator between the key and the value and each dict key.

Usage:

    tuple $source_list "=" "," | include "dict_to_key_values_separated_string"

If dict has key

Values:

    source_list:
      one: thing
      two: stuff

Template:

{{- tuple $source_list | include "dict_to_key_values_separated_string" }}

Result:

    one=thing,two=stuff

Template:

{{- tuple $source_list "=" "," | include "dict_to_key_values_separated_string" }}

Result:

    one=thing,two=stuff

Template:

{{- tuple $source_list ":" "|" | include "dict_to_key_values_separated_string" }}

Result:

    one:thing|two:stuff


If dict is empty

Values:

    source_list: {}

Template:

{{- tuple $source_list "=" "," | include "dict_to_key_values_separated_string" }}

result: 
    
    ""

*/}}


{{- define "dict_to_key_values_separated_string" -}}

{{- $dict := index . 0 -}}
{{- $key_value_separator := "=" -}}
{{- if gt (len .) 1 }}
  {{- $key_value_separator = index . 1 -}}
{{- end }}

{{- $list_separator := "," -}}
{{- if gt (len .) 2 }}
  {{- $list_separator = index . 2 -}}
{{- end }}


{{- $current_list := list -}}
{{- range $dict_key, $dict_value := $dict -}}
{{- $current_list = append $current_list (printf "%s%s%v" $dict_key $key_value_separator $dict_value) -}}
{{- end -}}


{{- join $list_separator $current_list -}}

{{- end -}}
