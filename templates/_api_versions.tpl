{{/*
This named template that defines apiVersion for some kind of resources
*/}}

{{- define "apiVersions" -}}
Cluster: cluster.x-k8s.io/v1beta1
MachineDeployment: cluster.x-k8s.io/v1beta1
KubeadmControlPlane: controlplane.cluster.x-k8s.io/v1beta1
KubeadmConfigTemplate: bootstrap.cluster.x-k8s.io/v1beta1
RKE2ControlPlane: controlplane.cluster.x-k8s.io/v1beta1
RKE2ConfigTemplate: bootstrap.cluster.x-k8s.io/v1beta1
OpenshiftAssistedControlPlane: controlplane.cluster.x-k8s.io/v1alpha1
OpenshiftAssistedConfigTemplate: bootstrap.cluster.x-k8s.io/v1alpha1
CK8sControlPlane: controlplane.cluster.x-k8s.io/v1beta2
CK8sConfigTemplate: bootstrap.cluster.x-k8s.io/v1beta2
DockerCluster: infrastructure.cluster.x-k8s.io/v1beta1
DockerMachineTemplate: infrastructure.cluster.x-k8s.io/v1beta1
VSphereCluster: infrastructure.cluster.x-k8s.io/v1beta1
VSphereMachineTemplate: infrastructure.cluster.x-k8s.io/v1beta1
OpenStackCluster: infrastructure.cluster.x-k8s.io/v1beta1
OpenStackMachineTemplate: infrastructure.cluster.x-k8s.io/v1beta1
Metal3Cluster: infrastructure.cluster.x-k8s.io/v1beta1
Metal3MachineTemplate: infrastructure.cluster.x-k8s.io/v1beta1
Metal3DataTemplate: infrastructure.cluster.x-k8s.io/v1beta1
BareMetalHost: metal3.io/v1alpha1
IPPool: ipam.metal3.io/v1alpha1
{{- end -}}


{{/*
This named template returns the apiVersion for the resource passed in argument

Note: a failure is raised if resource is not declared in previous "apiVersions" named template

*/}}

{{- define "getApiVersion" -}}
{{- $key := . -}}
{{- $allApiVersions := (include "apiVersions" . |fromYaml) -}}
{{- if hasKey $allApiVersions $key -}}
{{ get $allApiVersions $key }}
{{- else -}}
{{- fail (printf "api version for object %s not defined in sylva-library" $key)}}
{{- end -}}
{{- end -}}
